# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.1](https://github.com/renli-tech/Beyond/compare/@beyond-ui/react@1.1.0...@beyond-ui/react@1.1.1) (2021-08-19)

### Bug Fixes

- **react:** export all hooks and providers ([cde6155](https://github.com/renli-tech/Beyond/commit/cde6155adb864b861c192aa638bfad8a11aef1dd))

# [1.1.0](https://github.com/renli-tech/Beyond/compare/@beyond-ui/react@1.0.1...@beyond-ui/react@1.1.0) (2021-08-19)

### Features

- **react:** added README.md file ([262e624](https://github.com/renli-tech/Beyond/commit/262e624d969c56b905347b7df964bced936849e1))
- **react:** updated README.md file ([c73411b](https://github.com/renli-tech/Beyond/commit/c73411b16865408d8641b29de05f84686a2d1f5f))
